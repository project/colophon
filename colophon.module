<?php


/**
 * @file
 * List all enabled modules and themes in a 'colophon' or 'about' page 
 *
 * Based on code provided by somebody at: http://drupal.org/node/199755
 * Development sponsored by www.webnthings.co.za.
 * This module is for Drupal 5.x
 */

/*******************************************************************************
* Hook Functions (Drupal)
******************************************************************************/

/**
 * Implementation of hook_help().
 */
function colophon_help($section) {
  switch ($section) {
    case 'admin/settings/colophon':
      return '<a href="http://en.wikipedia.org/wiki/Colophon_%28publishing%29">WIKI</a><br /><p>'. t('  The colophon module will show you a list of all the enabled modules as well as themes in this installation. <br> You can add your own intro message and also notes for the modules and themes section each. <br> You are able to select if you you want to view themes or modules only. <br /> When you\'ve enabled the module, the colophon can be directly linked as "mysite.com/colophon". <br> You can put a link anywhere on your page by using the colophon block. A selection of Drupal icons and banners are available which you can use as a link inside the block.  You can also use your own text instead. <br> The text or icons can be positioned inside the block left, right or center.');
  }
}
        
/**
 * Implementation of hook_menu().
 */
function colophon_menu($may_cache) {
  drupal_add_css(drupal_get_path('module', 'colophon') . '/colophon.css');
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/colophon',
      'title' => t('Colophon settings'),
      'description' => t('Settings for the colophon or about page that shows enabled modules and themes'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('colophon_admin_settings'),
      'access' => user_access('administer site configuration'),
    );
    
    $items[] = array(
      'path' => 'colophon',
      'title' => t('Colophon'),
      'callback' => 'colophon_display',
      'type' => MENU_CALLBACK,
      'access' => TRUE,
    );
  }

  return $items;
}

/**
 * Implementation of hook_block().
 */
function colophon_block($op='list', $delta=0) {
  if ($op == "list") {
    $block[0]["info"] = t('Colophon');
    return $block;
  } 
  elseif ($op == "view") {
    if (variable_get('colophon_link', 0) == 0) {     
      $modpath = base_path() . drupal_get_path('module', 'colophon');
      $colophon_icons = array(
      $modpath.'/images/druplicon_small.jpg',
      $modpath.'/images/druplicon_eyes_small.jpg',
      $modpath.'/images/powered-blue-80x15.png',
      $modpath.'/images/powered-gray-80x15.png',
      $modpath.'/images/powered-black-80x15.png',
      $modpath.'/images/powered-blue-88x31.png',
      $modpath.'/images/powered-gray-88x31.png',
      $modpath.'/images/powered-black-88x31.png',
      $modpath.'/images/powered-blue-135x42.png',
      $modpath.'/images/powered-gray-135x42.png',
      $modpath.'/images/powered-black-135x42.png',
      $modpath.'/images/drupal.powered.stealthese.1.png',
      $modpath.'/images/drupal.powered.stealthese.2.png',
      $modpath.'/images/drupal.powered.3d.png',
      'Text only',
      );
      
      
      switch (variable_get('colophon_position', 1)) {
        case 0:
          if ($colophon_icons[variable_get('colophon_icon', 0)] == 'Text only') {
            $block_content .= '<div class="left_image"><a href="' . $base_url . '/colophon" title="Colophon">'. variable_get('colophon_text', 'Powered by Drupal') . '</div></a>'; 
          }
          else{
            $block_content .= '<a href="' . $base_url . '/colophon" title="Colophon"><img src="'. $colophon_icons[variable_get('colophon_icon', 0)] . '" class="left_image" alt="Powered by Drupal"></a>'; 
          }
          break;
        
        case 1:
          if ($colophon_icons[variable_get('colophon_icon', 0)] == 'Text only') {
            $block_content .= '<div class="center_image"><a href="' . $base_url . '/colophon" title="Colophon">'. variable_get('colophon_text', 'Powered by Drupal') . '</div></a>'; 
          }
          else{
            $block_content .= '<a href="' . $base_url . '/colophon" title="Colophon"><img src="'. $colophon_icons[variable_get('colophon_icon', 0)] . '" class="center_image" alt="Powered by Drupal"></a>'; 
          }
          break;
        
        case 2:
          if ($colophon_icons == 'Text only') {
            $block_content .= '<div class="right_image"><a href="' . $base_url . '/colophon" title="Colophon">'. variable_get('colophon_text', 'Powered by Drupal') . '</div></a>'; 
          }
          else{
            $block_content .= '<a href="' . $base_url . '/colophon" title="Colophon"><img src="'. $colophon_icons[variable_get('colophon_icon', 0)] . '" class="right_image" alt="Powered by Drupal"></a>'; 
          }
      }    
    }
  $block['subject'] = '';
  $block['content'] = $block_content;
  return $block;
  }
}
  

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/
 function colophon_admin_settings() {
  $pathmod = base_path() . drupal_get_path('module', 'colophon');
  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content.'),
  );
  
  $form['content']['colophon_option'] = array(
    '#type' => 'checkboxes',
    '#title' => t('View modules and themes'),
    '#description' => t('Select whether to show modules or themes or both.'),
    '#default_value' => variable_get('colophon_option', array('modules', 'themes')),
    '#options' => array(
      'modules' => t('modules'),
      'themes' => t('themes'),
    ),
  );
  
  $form['content']['colophon_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment'),
    '#description' => t('General comments or notes regarding this Drupal installation.'),
    '#default_value' => variable_get('colophon_intro', ''),
    '#rows' => 3,
  );
  
  $form['content']['colophon_module_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment for modules'),
    '#description' => t('Comments or notes regarding the modules in this Drupal installation.'),
    '#default_value' => variable_get('colophon_module_intro', ''),
    '#rows' => 3,
  );
  
  $form['content']['colophon_theme_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment for themes'),
    '#description' => t('Comments or notes regarding the themes in this Drupal installation.'),
    '#default_value' => variable_get('colophon_theme_intro', ''),
    '#rows' => 3,
  );
  
  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Icons and position.'),
  );
  $form['display']['colophon_icon'] = array(
    '#type' => 'radios',
    '#title' => t('Select icon'),
    '#description' => t('Select icon or text to be displayed in footer'),
    '#default_value' => variable_get('colophon_icon', 0),
    '#options' => array(
      '<img src="'. $pathmod .'/images/druplicon_small.jpg" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/druplicon_eyes_small.jpg" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-blue-80x15.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-gray-80x15.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-black-80x15.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-blue-88x31.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-gray-88x31.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-black-88x31.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-blue-135x42.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-gray-135x42.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/powered-black-135x42.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/drupal.powered.stealthese.1.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/drupal.powered.stealthese.2.png" style="position: relative; left: 2.5em;">',
      '<img src="'. $pathmod .'/images/drupal.powered.3d.png" style="position: relative; left: 2.5em;">',
      t('Text only') ,
    )
  );
  
  $form['display']['colophon_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text in footer'),
    '#description' => t('If you selected Text only to be displayed in footer, enter it here.'),
    '#default_value' => variable_get('colophon_text', 'Powered by Drupal'),       
  );
  
  $form['display']['colophon_position'] = array(
    '#type' => 'radios',
    '#title' => t('Alignment'),
    '#description' => t('Position where icon or text to be displayed in footer'),
    '#default_value' => variable_get('colophon_position', 1),
    '#options' => array(
      t('Left'),
      t('Center'),
      t('Right'),
    )
  );
  
  return system_settings_form($form);
}

  function colophon_display() {
  $options = variable_get('colophon_option',  array('modules', 'themes'));
  $content = '<div id="colophon">';
  $content .= '<h2 class="title">Drupal modules and themes used in ' . variable_get('site_name', '') . '</h2><br>';
  $content .='<table><tr><td><a href="http://www.drupal.org" title="Drupal Home"><img src="'. $base_url . '/misc/druplicon.png" class="left_image" alt="Drupal home"><a></td><td>';
  $content .= '<a href="http://www.drupal.org" title="Drupal Home"><h3 class ="version">Drupal version: '. VERSION . '</h3><a><br></td></tr</table>';
  $content .= '<div class="intro">' . variable_get('colophon_intro', '') . '</div><br>';
  // Modules
  if ($options['modules']) {
    $modules = module_list(FALSE, FALSE, TRUE);
     $content .= '<h4>Modules:</h4><br />';
    $content .= '<div class="module_intro">' . variable_get('colophon_module_intro', '') . '</div>';
    $content .= '<table><thead><th>Name</th><th>Version</th><th>Description</th><th>URL</th></thead><tbody>';
    $row = 0;
    foreach ($modules as $item) {
      if (module_exists($item)) { // Only list the module if it's enabled
        $module_loc = drupal_get_filename('module', $item); // The location of the Module
        $info = parse_ini_file(dirname($module_loc) . '/' . $item . '.info'); // Location of the info file
        $name = $info['name'];
        $description = $info['description'];
        $version = $info['version'];
        $project = $info['project'];
        $package = $info['package'];
        $togs =($row % 2 == 0 ? 'odd' : 'even');
        $content .= '<tr class = ' . $togs . '><td>' . $name;
        if ($package) {
          $content .= '<br /><small>(Part of ' . $package . ')';
        }
        $content .= '</td><td>' . $version . '</td><td>' . $description . '</td><td><a href="http://drupal.org/project/' . $project . '">http://drupal.org/project/' . $project . '</a></td></tr>';
        $row++;
      }
    }
    $content .= '</table>';
  }

  // Themes
  if ($options['themes']) {
    $themes = list_themes();
    $content .= '<h4>Themes:</h4><br />';
    $content .= '<div class="theme_intro">' . variable_get('colophon_theme_intro', '') . '</div>';
    $content .= '<table ><thead><th style: width="60%">Screenshot</th><th style: width="40%">Name</th></thead>';
    $row = 0;
    foreach ($themes as $item) {
      // Only lists the theme if the theme is enabled
      $list = get_object_vars($item); // Drupal's list_themes() function returns an array of objects, so we extract an array from each of the objects
      if ($list['status']) {  // Only list the theme if it is enabled
        $name = $list['name'];
        if (file_exists(dirname($list['filename']) . '/screenshot-drupal.org.png')) { // List the screenshot intended for drupal.or, if available
          $screenshot = dirname($list['filename']) . '/screenshot-drupal.org.png';
        } 
        else {
          $screenshot = dirname($list['filename']) . '/screenshot.png';
        }
        $togs =($row % 2 == 0 ? 'odd' : 'even');
        $content .= '<tr class = ' . $togs . '><td><img src="' . $screenshot . '" alt="' . $name . ' screenshot" /></td><td> 	&nbsp;' . $name . '</td></tr>';
        $row++;
      }
    }
    $content .= '</table>';
    $content .= '</div>';
  }
    return $content;
  }

  function theme_colphon_footer($message) {
    return '<div id="footer">'. $message .'</div>';
  }
  
/*******************************************************************************
 * Module and Helper Functions
 ******************************************************************************/

